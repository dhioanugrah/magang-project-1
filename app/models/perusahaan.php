<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class perusahaan extends Model
{
    protected $table = 'perusahaans';

    protected $fillable = [
       'nama','lokasi','tahun','link_id',
    ];

    protected $guarded = [
        'id','updated_at','created_at'
    ];
    
    public function table_links()
    {
     return $this->belongsTo(table::class, 'link_id');   
    }
}
