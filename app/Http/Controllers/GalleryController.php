<?php

namespace App\Http\Controllers;
use App\models\gallery;
use Illuminate\Http\Request;

class GalleryController extends Controller
{
    public function index()
    {
        $gallery = gallery::all();
        return view('gallery', compact('gallery'));

    }
    public function create()
    {
        return view('formgallery');
    }

    public function upload(){
		return view('upload');
	}
 
	public function store(Request $request) {
    
		$this->validate($request, [
			'file' => 'required',
			'nama' => 'required',
		]);
 
		// menyimpan data file yang diupload ke variabel $file
		$file = $request->file('file');
        
 
      	        // nama file
		echo 'File Name: '.$file->getClientOriginalName();
		echo '<br>';
 
      	        // ekstensi file
		echo 'File Extension: '.$file->getClientOriginalExtension();
		echo '<br>';
 
      	        // real path
		echo 'File Real Path: '.$file->getRealPath();
		echo '<br>';
 
      	        // ukuran file
		echo 'File Size: '.$file->getSize();
		echo '<br>';
 
      	        // tipe mime
		echo 'File Mime Type: '.$file->getMimeType();
 
      	        // isi dengan nama folder tempat kemana file diupload
 		$tujuan_upload = 'data_file';
 
                // upload file
		$file->storeAs('public/gallery', $file->getClientOriginalName());
	}
}
//     public function create()
//     {
//         return view('form');
//     }
//     public function store(Request $request){
        
//         $validateData = $request->validate([
//             'nama'  => 'required',
//             'url'   => 'required'
//         ]);
//         $link = table::create([
//             'nama' => $request->nama,
//             'url' => $request->url
//         ]);
//         return redirect()->route('link.index');
//     }
//     public function edit($id)
//     {
//         $ubah = table::findorfail($id);
//         return view('edit',compact('ubah'));        
//     }
//     public function update(Request $request, $id)
//     {
//         $ubah = table::findorfail($id);
//         $ubah->update($request->all());
//         return redirect()->route('link.index');
//     }
//     public function destroy($id)
//     {
//         $ubah = table::findorfail($id);
//         $ubah->delete();
//         return back();
//     }
// }
// }
