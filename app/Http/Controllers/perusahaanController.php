<?php

namespace App\Http\Controllers;
use App\models\perusahaan;
use App\models\table;


use Illuminate\Http\Request;

class perusahaanController extends Controller
{
    public function index()
    {
        $usaha = perusahaan::all();
      
        return view('perusahaan', compact('usaha'));

    }
    public function create()
    {
        $table = table::all();
        return view('formperusahaan', compact('table'));
    }
    public function store(Request $request){
        
        $validateData = $request->validate([
            'nama'  => 'required',
            'lokasi'   => 'required',
            'tahun'   => 'required',
            'link_id'   => 'required'
        ]);
        $perusahaan = perusahaan::create([
            'nama' => $request->nama,
            'lokasi' => $request->lokasi,
            'tahun' => $request->tahun,
            'link_id' => $request->link_id
        ]);
        return redirect()->route('perusahaan.index');
    }
    public function edit($id)
    {
        
        $ubahperusahaan = perusahaan::findorfail($id);
        return view('editperusahaan',compact('ubahperusahaan'));        
    }
    public function update(Request $request, $id)
    {
        $ubahperusahaan = perusahaan::findorfail($id);
        $ubahperusahaan->update($request->all());
        return redirect()->route('perusahaan.index');
    }
    public function destroy($id)
    {
        $ubahperusahaan = perusahaan::findorfail($id);
        $ubahperusahaan->delete();
        return back();
    }
}


