<?php

use Illuminate\Support\Facades\Route;
use app\models\link;

use App\Http\Controllers\LinkController;
use App\Http\Controllers\perusahaanController;
use App\Http\Controllers\GalleryController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome' );

});
Route::get('/home', function () {
    return view('home' );

});

Route::get('/data', [LinkController::class, 'index'])->name('link.index');
Route::post('/form',  [LinkController::class, 'store'])->name('store.category');
Route::get('/form/create',  [LinkController::class, 'create'])->name('link.create');
Route::get('/edit/{id}',  [LinkController::class, 'edit'])->name('link.edit');
Route::get('/delete/{id}',  [LinkController::class, 'destroy'])->name('delete.edit');
Route::get('/update/{id}',  [LinkController::class, 'update'])->name('link.update');


Route::get('/perusahaan', [perusahaanController::class, 'index'])->name('perusahaan.index');
Route::post('/formperusahaan',  [perusahaanController::class, 'store'])->name('perusahaan.store');
Route::get('/formperusahaan/create',  [perusahaanController::class, 'create'])->name('perusahaan.create');
Route::get('/editperusahaan/{id}',  [perusahaanController::class, 'edit'])->name('perusahaan.edit');
Route::get('/deleteperusahaan/{id}',  [perusahaanController::class, 'destroy'])->name('delete.edit');
Route::get('/updateperusahaan/{id}',  [perusahaanController::class, 'update'])->name('perusahaan.update');

Route::get('/gallery', [GalleryController::class, 'index'])->name('gallery.index');
Route::post('/formgallery',  [GalleryController::class, 'store'])->name('gallery.store');
Route::get('/formgallery/create',  [GalleryController::class, 'create'])->name('gallery.create');
Route::get('/editgallery/{id}',  [GalleryController::class, 'edit'])->name('gallery.edit');
Route::get('/deletegallery/{id}',  [GalleryController::class, 'destroy'])->name('gallery.delete');
Route::get('/updategallery/{id}',  [GalleryController::class, 'update'])->name('gallery.update');
