<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        <title>HOME</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        <div class=" mx-auto bg-blue-500 p-5">
            <nav class="flex justify-between">
                <div class="text-white">
                    <a href="#">Dhio Anugrah</a>
                </div>
                <ul class="flex flex-row text-white">
                    <li class="pr-5">
                        <a href="/home"> HOME </a>
                    </li>
                    <li class="pr-5">
                        <a href="/data">LINK</a>
                    </li>
                    <li class="pr-5">
                        <a href="/perusahaan">PERUSAHAAN</a>
                    </li>
                </ul>
            </nav>
        </div>
        <div class="flex-center position-ref full-height bg-gray-300">
           
            <div class="content">
                <div class="title m-b-md text-red-500 ">
                  HOME
                </div>

               
            </div>
        </div>
    </body>
</html>
