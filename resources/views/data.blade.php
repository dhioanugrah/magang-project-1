<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        <title>Data Link</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }   

            .title {
                font-size: 84px;
            }


            .m-b-md {
                margin-bottom: 30px;
            }
        
           #table {
            font-family: Arial, Helvetica, sans-serif;
         
             width: 100%;
        
            }

            #table td, #table th {
            border: 1px solid #A9A9A9;
            padding: 8px;
            color: black;
            }

            #table tr:nth-child(even){background-color: #A9A9A9;}

            #table th {
            padding-top: 12px;
            padding-bottom: 12px;
            text-align: left;
            background-color: #696969;
            color: white;
            }

           
        </style>        
    </head>
    <body>
        <div class=" mx-auto bg-blue-500 p-5">
            <nav class="flex justify-between">
                <div class="text-white">
                    <a href="#">Dhio Anugrah</a>
                </div>
                <ul class="flex flex-row text-white">
                    <li class="pr-5">
                        <a href="/home" class="text-white px-4 py-2 rounded-md  hover:bg-blue-300 hover:text-blue-600"> HOME </a>
                    </li>
                    <li class="pr-5">
                        <a href="/data"  class="text-white px-4 py-2 rounded-md  hover:bg-blue-300 hover:text-blue-600">LINK</a>
                    </li>
                    <li class="pr-5">
                        <a href="/perusahaan"  class="text-white px-4 py-2 rounded-md  hover:bg-blue-300 hover:text-blue-600">PERUSAHAAN</a>
                    </li>
                </ul>
            </nav>
        </div>
        
        <div class="flex-center position-ref full-height bg-gray-600">
            <div class="content">
                <div class="title m-b-md text-blue-600 ">
                    LINK
                </div>
                     <table id="table" class="mb-5 bg-gray-400">
                            <thead>
                                <th>NO</th>
                               
                                <th>Nama</th>
                                <th>Url</th>
                                <th>Aksi</th>
                            </thead>
                          
                                @php
                                    $i = 1;
                                @endphp
                            
                                @foreach ( $table as $d )
                                <tr>
                                    <td>{{ $i++ }}</td>
                                    <td>{{ $d->nama }}</td>
                                    {{-- <td>{{ $d->url }}</td> --}}
                                    <td>
                                        <a href="http://{{ $d->url }}">
                                            {{ $d->url }}
                                        </a>
                                    </td>
                                    <td>
                                        <a href="{{ url('edit',$d->id) }}"  class="text-blue-500 px-4 py-2 rounded-md border-blue-500 border hover:bg-blue-500 hover:text-white">
                                            Edit
                                        </a>|
                                        <a href="{{ url('delete',$d->id) }}" class="text-red-500  px-4 py-2 rounded-md border-red-500 border hover:bg-red-500 hover:text-white">
                                            Delete
                                        </a>

                                    </td>
                                </tr>
                                @endforeach

                                
                        </table>
                    <!-- <button class=" bg-gray-800 p-1 rounded text-blue-300">TAMBAH LINK</button> -->
               <a href="{{ route('link.create') }}">
                <button class="text-blue-500  px-4 py-2 rounded-md border-blue-500 border hover:bg-blue-900 hover:text-white">TAMBAH LINK</button>
               </a>               
            </div>
        
        </div>
        
        </body>
</html>
