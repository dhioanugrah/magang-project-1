<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <title>Form perusahaan</title>
</head>
<body>

    <div class="min-h-screen flex items-center justify-center bg-gray-900" >

    <div class="bg-white p-10 rounded shadow-2x1 w-1/2">


        <form action= "{{ route('perusahaan.store') }}" method='post'>
        @csrf
            <div class="mb-8" > 
                <label class="block mb-2 font-bold ">Perusahaan</label>
                <input type="text" name="nama" class="w-full border-2 border-gray-300 rounded outline-none focus:border-blue-600  ">             
            </div>

            <div class="mb-8" >
                <label class="block mb-2 font-bold ">Lokasi</label>
                <input type="text" name="lokasi" class="w-full border-2 border-gray-300 rounded outline-none focus:border-blue-600  ">
            </div>
            <div class="mb-8" >
                <label class="block mb-2 font-bold ">Tahun</label>
                <input type="text" name="tahun" class="w-full border-2 border-gray-300 rounded outline-none focus:border-blue-600  ">
            </div>
            <div class="mb-8">
                <label class="block mb-2 font-bold">Link</label>
                <select name="link_id"  class="form-control  bg-gray-400 text-white">
                    <option value="">- pilih -</option>

                    @foreach ($table as $item)
                        
                    <option value="{{ $item->id }}">{{ $item->url }}</option>

                    @endforeach
                </select> 
            </div>

            <button class=" text-blue-500  px-4 py-2 rounded-md border-blue-500 border hover:bg-blue-900 hover:text-white">Tambah</button>


        </form>

    </div>
    
    </div>
</body>
</html>     